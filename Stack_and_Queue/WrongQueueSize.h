#pragma once

#include <exception>

class WrongQueueSize : public std::exception
{
public:
	WrongQueueSize() : reason_("Wrong queue size!") {}
	const char* what() const noexcept override { return reason_; }
private:
	const char* reason_;
};