#pragma once

#include <exception>

class WrongStackSize : public std::exception
{
public:
	WrongStackSize() : reason_("Wrong stack size!") {}
	const char* what() const noexcept override { return reason_; }
private:
	const char* reason_;
};