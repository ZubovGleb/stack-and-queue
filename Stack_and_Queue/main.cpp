#include "LimitedSizeStack.h"
#include "LimitedSizeQueue.h"
#include <iostream>
#include <iomanip>

bool checkBalanceBrackets(const char* text, const int maxDeep = 50);

int main() {
    std::cout << "-----------Stack testing-----------\n";
    try //stack testing
    {
        StackArray<int> stack1(3);
        std::cout << "Is stack1 empty? ";
        stack1.isEmpty() ? std::cout << "Yes\n" : std::cout << "No\n";
        stack1.push(7);
        stack1.push(1);
        stack1.push(4);
        std::cout << stack1.pop() << "\n";
        std::cout << stack1.pop() << "\n";
        std::cout << stack1.pop() << "\n";
        std::cout << "Is stack1 empty? ";
        stack1.isEmpty() ? std::cout << "Yes\n" : std::cout << "No\n";
       
        std::cout << '\n';

        std::cout << "Balance of brackets:\n";
        const char* text1 = " good ";
        std::cout << std::setw(50) << std::left << text1 << (checkBalanceBrackets(text1) ? "right" : "wrong") << "\n";
        const char* text2 = "( ) good ";
        std::cout << std::setw(50) << std::left << text2 << (checkBalanceBrackets(text2) ? "right" : "wrong") << "\n";
        const char* text3 = "( [ {} ] ) good ";
        std::cout << std::setw(50) << std::left << text3 << (checkBalanceBrackets(text3) ? "right" : "wrong") << "\n";
        const char* text4 = "( { [ { } [ ] { [ ] } ] } ) good";
        std::cout << std::setw(50) << std::left << text4 << (checkBalanceBrackets(text4) ? "right" : "wrong") << "\n";
        const char* text5 = "( ( [{ }[ ]({ })] ) extra left parenthesis ";
        std::cout << std::setw(50) << std::left << text5 << (checkBalanceBrackets(text5) ? "right" : "wrong") << "\n";
        const char* text6 = "( ( [{ ][ ]([ ])] ) } ) unpaired brackets ";
        std::cout << std::setw(50) << std::left << text6 << (checkBalanceBrackets(text6) ? "right" : "wrong") << "\n";
        std::cout << '\n';
    }
    catch (const std::exception& error)
    {
        std::cerr << "Error: " << error.what() << '\n';
        return 1;
    }
    std::cout << "-----------Queue testing-----------\n";
    try //queue testing
    {
        QueueArray<int> queue1(5);
        std::cout << "Add 1 2 3 4 5 to queue1\n";
        queue1.enQueue(1);
        queue1.enQueue(2);
        queue1.enQueue(3);
        queue1.enQueue(4);
        queue1.enQueue(5);
        std::cout << "queue1: ";
        std::cout << queue1 << '\n';
        std::cout << "Is queue1 empty? ";
        std::cout << (queue1.isEmpty() ? "Yes\n" : "No\n");

        std::cout << "Delete four elements from queue1\n";
        queue1.deQueue();
        queue1.deQueue();
        queue1.deQueue();
        queue1.deQueue();
        std::cout << "queue1: ";
        std::cout << queue1 << '\n';
        std::cout << "Is queue1 empty? ";
        std::cout << (queue1.isEmpty() ? "Yes\n" : "No\n");

        std::cout << "Delete one element from queue1.\n";
        queue1.deQueue();
        std::cout << "queue1: ";
        std::cout << queue1 << '\n';
        std::cout << "Is queue1 empty? ";
        std::cout << (queue1.isEmpty() ? "Yes\n" : "No\n");
        std::cout << "\n";

        QueueArray<int> queue2(5);
        std::cout << "Add 6 7 8 9 10 to queue2.\n";
        queue2.enQueue(6);
        queue2.enQueue(7);
        queue2.enQueue(8);
        queue2.enQueue(9);
        queue2.enQueue(10);

        std::cout << "queue2: ";
        std::cout << queue2 << '\n';
        std::cout << "Delete two elements from queue2\n";
        queue2.deQueue();
        queue2.deQueue();
        std::cout << "Add 11 12 to queue2.\n";
        queue2.enQueue(11);
        queue2.enQueue(12);
        std::cout << "queue2: ";
        std::cout << queue2 << '\n';
        std::cout << queue2.deQueue() << '\n';
        std::cout << queue2.deQueue() << '\n';
        std::cout << queue2.deQueue() << '\n';
        std::cout << queue2.deQueue() << '\n';
        std::cout << queue2.deQueue() << '\n';
        std::cout << "Is queue2 empty: ";
        std::cout << (queue2.isEmpty() ? "Yes" : "No") << "\n";
    }
    catch (const std::exception& error)
    {
        std::cerr << "Error: " << error.what() << '\n';
        return 2;
    }
    return 0;
}

bool checkBalanceBrackets(const char* text, int maxDeep)
{
    char temp;
    StackArray<char> testStack(maxDeep);
    int countPairedBrackets = 0;
    while (*text != '\0')
    {
        if (*text == '(' || *text == '[' || *text == '{')
        {
            testStack.push(*text);
            text++;
            countPairedBrackets++;
        }
        else if (*text == ')' || *text == ']' || *text == '}')
        {
            countPairedBrackets--;
            if (countPairedBrackets < 0)
            {
                return false;
            }
            temp = testStack.pop();
            if (!(temp == '(' && *text == ')' || temp == '[' && *text == ']' || temp == '{' && *text == '}'))
            {
                return false;
            }
            text++;
        }
        else
        {
            text++;
        }
    }
    if (countPairedBrackets != 0)
    {
        return false;
    }
    return true;
}